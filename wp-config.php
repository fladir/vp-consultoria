<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'vpconsultoria' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'SlN2#/V/mDQ&^@]q;dDP2+{qne[FovTj(YO $h~U.L_[Y]$ydWki& b#w/%o$,X6' );
define( 'SECURE_AUTH_KEY',  'HZL_)q<f?AM tR70Xr~n7aQhnv=wOD`[N S{J:}YX)}ojO?A/[GE&j]QIfH)3^bn' );
define( 'LOGGED_IN_KEY',    '1!Qv6,Ziafs;BV3,/4 LC9HCbt4ZeA}wcO4%68q9CU7AK%w/$gz}D^U0Ivgn94P7' );
define( 'NONCE_KEY',        '}iagN!&n7NmO6j OA:npxxCMk#$h}TpzXGSt+W@8_g^lIdTDjH4o-&#m<zk[sg[u' );
define( 'AUTH_SALT',        'Nh:?b&:B.`&I+{`K#_%x.|+29<8[2%cT9-vCCwDdaUmx]`BkYs`+|{]v#5fi$u#p' );
define( 'SECURE_AUTH_SALT', 'ii,2u2ZOUgb{=I-!,13va?k7P)Uvf&*U_.nVag[aNVsyt>^La.c.#e]XF0<yt(Um' );
define( 'LOGGED_IN_SALT',   'lfxa)>(i8:~x7euCeTJBF #I=FKTHm4^=)MK),vN@YLJ6iaSL1@#aI,hYLD4fLY$' );
define( 'NONCE_SALT',       'LX#Y$W)q]3[9#=N4O}q4zg)XYpt$jrJp9=Vr6%2v-K2c5HvQL.w+?m_,*_>t7>o@' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');
