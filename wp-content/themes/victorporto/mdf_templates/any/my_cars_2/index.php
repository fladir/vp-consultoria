<?php if (!defined('ABSPATH')) die('No direct access allowed'); ?>
<?php
global $mdf_loop;
MDTF_SORT_PANEL::mdtf_catalog_ordering();

?>





    <section class="our-listing bgc-f7 pb30-991">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="breadcrumb_content style2 mb0-991">
                        <?php
                        if (function_exists('yoast_breadcrumb')) {
                            yoast_breadcrumb('<p id="breadcrumbs-item">', '</p>');
                        }
                        ?>


                        <h2 class="breadcrumb_title">            <div class="mdf_widget_found_count"><?php
                                $show_items_count_text = __('Encontramos <span>%s</span> imóveis', 'meta-data-filter');
                                if (isset($shortcode_options['options']['show_items_count_text']) AND ! empty($shortcode_options['options']['show_items_count_text']))
                                {
                                    $show_items_count_text = str_replace('%s', '<span>%s</span>', $shortcode_options['options']['show_items_count_text']);
                                }
                                //***
                                echo sprintf(__($show_items_count_text), (isset($_REQUEST['meta_data_filter_count']) ? $_REQUEST['meta_data_filter_count'] : 0));
                                ?></div>
                        </h2>
                    </div>
                </div>


                <div class="col-lg-6">
                    <div class="listing_list_style mb20-xsd tal-991 d-none">
                        <ul class="mb0">
                            <li class="list-inline-item"><a href="#"><span class="fa fa-th-large"></span></a></li>
                            <li class="list-inline-item"><a href="#"><span class="fa fa-th-list"></span></a></li>
                        </ul>
                    </div>
                    <div class="dn db-991 mt30 mb0">
                        <div id="main2">
                                <span id="open2"
                                      class="flaticon-filter-results-button filter_open_btn style2"> Show Filter</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-xl-4">

                    <?php echo do_shortcode('[mdf_search_form id="270"]'); ?>


                    <div class="sidebar_listing_grid1 dn-991">
                        <div class="terms_condition_widget">
                            <h3 class="title">Imóveis em Destaque</h3>
                            <div class="sidebar_feature_property_slider">
                                <?php
                                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                $args = array(
                                    'nopaging' => false,
                                    'paged' => $paged,
                                    'post_type' => 'imovel',
                                    'posts_per_page' => 5,
                                    'orderby' => 'post_date',
                                    'order' => 'DESC'
                                );

                                $WPQuery = new WP_Query($args);

                                ?>
                                <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>

                                    <div class="item">
                                        <div class="feat_property home7">
                                            <div class="thumb">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail('imoveis_list', array('class' => 'img-whp', 'alt' => '' . get_the_title() . '')); ?>
                                                </a>
                                                <div class="thmb_cntnt">
                                                    <a class="fp_price  mb-3"
                                                       href="#">R$ <?php echo get_field('preco') ?>
                                                        <small><?php echo $term->name === 'Aluguel' ? '/mês' : '' ?></small></a>
                                                    <h4 class="posr color-white"><?php the_title() ?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php endwhile; endif; ?>

                            </div>
                        </div>
                        <div class="terms_condition_widget">
                            <h3 class="title">Tipos de Imóveis</h3>
                            <div class="widget_list">
                                <ul class="list_details">
                                    <?php
                                    $categories = get_terms(
                                        array(
                                            'taxonomy'   => 'imoveis',
                                            'hide_empty' => false,
                                        )
                                    );
                                    foreach ($categories as $cat) {
                                        echo "<li><a href=\"" . esc_url( get_term_link( $term ) ) . "\"><i class=\"fa fa-caret-right mr10\"></i>" . $cat->name . "<span class='float-right'>" . $cat->category_description . "  $cat->count   imóveis </span></a></li>";
                                    }
                                    ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-8">
                    <div class="row">


                        <?php
                        while ($mdf_loop->have_posts()) : $mdf_loop->the_post();
                        ?>


                            <div class="col-lg-12">
                                <div class="feat_property list">
                                    <div class="thumb">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail('imoveis_list', array('class' => 'img-whp', 'alt' => '' . get_the_title() . '')); ?>
                                        </a>

                                        <div class="thmb_cntnt">
                                            <ul class="icon mb0">
                                                <li class="list-inline-item"><a href="#"><span
                                                                class="flaticon-transfer-1"></span></a></li>
                                                <li class="list-inline-item"><a href="#"><span
                                                                class="flaticon-heart"></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="details">
                                        <div class="tc_content">
                                            <div class="dtls_headr">
                                                <ul class="tag">
                                                    <li class="list-inline-item comercializacao-tag d-none"><a
                                                                href="#">
                                                            <?php // Get terms for post
                                                            $terms = get_the_terms($post->ID, 'bairros');
                                                            // Loop over each item since it's an array
                                                            if ($terms != null) {
                                                                foreach ($terms as $term) {
                                                                    // Print the name method from $term which is an OBJECT
                                                                    echo '<span>';
                                                                    print $term->name;
                                                                    echo '</span> ';
                                                                    // Get rid of the other data stored in the object, since it's not needed
                                                                    unset($term);
                                                                }
                                                            } ?>
                                                        </a></li>


                                                    <?php if (get_field('destaque') == 1) : ?>
                                                        <li class="list-inline-item destaque-tag">
                                                            <a>
                                                                Destaque
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>


                                                </ul>
                                                <a class="fp_price" href="#">R$ <?php echo get_field('preco') ?>
                                                    <small><?php echo $term->name === 'Aluguel' ? '/mês' : '' ?></small></a>
                                            </div>
                                            <p class="text-thm">
                                                <?php // Get terms for post
                                                $terms = get_the_terms($post->ID, 'imoveis');
                                                // Loop over each item since it's an array
                                                if ($terms != null) {
                                                    foreach ($terms as $term) {
                                                        // Print the name method from $term which is an OBJECT
                                                        echo '<span>';
                                                        print $term->name;
                                                        echo '</span> ';
                                                        // Get rid of the other data stored in the object, since it's not needed
                                                        unset($term);
                                                    }
                                                } ?>
                                            </p>
                                            <a title="Saiba mais sobre <?php the_title() ?>"
                                               href="<?php the_permalink(); ?>"><h4><?php the_title() ?></h4></a>
                                            <p>
                                                <span class="flaticon-placeholder"></span> <?php if (get_field('endereco')): ?>
                                                    <?php echo get_field('endereco') ?>,
                                                <?php endif; ?>


                                                <?php
                                                $bairros = get_field('bairro');
                                                if ($bairros): ?>
                                                    <?php foreach ($bairros as $bairro): ?>
                                                        <strong>Bairro:</strong> <?php echo $bairro ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>

                                                <?php echo get_field('regiao') ?>

                                                <?php echo get_field('cep') ?>

                                                <?php if (get_field('cidade')): ?>
                                                    <strong>Cidade:</strong> <?php echo get_field('cidade') ?>
                                                <?php endif; ?><?php echo get_field('estado') ?></p>
                                            <ul class="prop_details mb0">
                                                <li class="list-inline-item"><a
                                                            href="#">Área: <?php echo get_field('area') ?> m²</a></li>
                                                <li class="list-inline-item"><a
                                                            href="#">Quartos: <?php echo get_field('quartos') ?></a>
                                                </li>
                                                <li class="list-inline-item"><a
                                                            href="#">Banheiros: <?php echo get_field('banheiros') ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?php
                        endwhile;

                        wp_reset_query() ?>

                        <div class="col-lg-12 mt20">
                            <div class="mbp_pagination">
                                <?php echo bootstrap_pagination($WPQuery); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


