<?php if (!defined('ABSPATH')) die('No direct access allowed'); ?>
<?php
//wp_enqueue_style('my_cars_2', get_template_directory_uri() . '/mdf_templates/any/my_cars_2/css/styles.css');
global $mdf_loop;
MDTF_SORT_PANEL::mdtf_catalog_ordering();
?>
<div class="add-listing">
    <button onclick="window.location.href = '<?php echo home_url() ?>/listings-cities/listings/add-listing/';" type="button" class="dr_button add_listing_btn">Add Listing</button>
</div>
<div class="clear"></div>
<div id="dr_listing_list">
    <div id="dr_listing_list">

        <?php
        while ($mdf_loop->have_posts()) : $mdf_loop->the_post();
            // Retrieves categories list of current post, separated by commas.
            $categories_list = get_the_category_list(__(', ', DR_TEXT_DOMAIN), '');

            // Retrieves tag list of current post, separated by commas.
            $tags_list = get_the_tag_list('', __(', ', DR_TEXT_DOMAIN), '');

            //add last css class for styling grids
            if ($count == $last)
                $class = 'dr_listing last-listing';
            else
                $class = 'dr_listing';
            ?>

            <div class="<?php echo $class ?>">
                <div id="post-<?php //the_ID();   ?>" <?php post_class(); ?>>

                    <div class="entry-post">

                        <div class="entry-meta">
                            <?php //the_dr_posted_on(); ?>
                            <div class="entry-utility">
                                <?php if ($categories_list): ?>
                                    <span class="cat-links"><?php echo sprintf(__('<span class="%1$s">Posted in</span> %2$s', DR_TEXT_DOMAIN), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list); ?></span><br />
                                    <?php
                                    unset($categories_list);
                                endif;
                                if ($tags_list):
                                    ?>
                                    <span class="tag-links"><?php echo sprintf(__('<span class="%1$s">Tagged</span> %2$s', DR_TEXT_DOMAIN), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list); ?></span><br />
                                    <?php
                                    unset($tags_list);
                                endif;
                                do_action('sr_avg_ratings_of_listings', get_the_ID());
                                ?>
                                <br /><span class="comments-link"><?php comments_popup_link(__('Leave a review', DR_TEXT_DOMAIN), __('1 Review', DR_TEXT_DOMAIN), esc_attr__('% Reviews', DR_TEXT_DOMAIN), '', __('Reviews Off', DR_TEXT_DOMAIN)); ?></span>
                            </div>
                        </div>

                        <div class="entry-summary">

                            <?php if (has_post_thumbnail()): ?>
                                <a href="<?php the_permalink(); ?>">
                                    <?php the_post_thumbnail(array(50, 50), array('class' => 'alignleft dr_listing_image_listing', 'title' => get_the_title(),)); ?></a>
                                <h2 class="entry-title">
                                    <a href="<?php echo the_permalink(); ?>" title="<?php echo sprintf(esc_attr__('Permalink to %s', DR_TEXT_DOMAIN), get_the_title()); ?>" rel="bookmark"><?php the_title(); ?></a>
                                </h2>
                                <?php echo do_shortcode('[ct id="_ct_text_5490854c8d875" property="title | description | value"]'); ?>
                                <br/>

                                <?php echo do_shortcode('[ct id="_ct_text_549085ef71d07" property="title | description | value"]'); ?> ,

                                <?php echo do_shortcode('[ct id="_ct_selectbox_54a1ca6949edd" property="title | description | value"]'); ?>

                                <?php echo do_shortcode('[ct id="_ct_text_5495dbff528f2" property="title | description | value"]'); ?>
                            <?php endif; ?>
                        </div>
                        <div class="clear"></div>
                    </div><!-- .entry-post -->

                    <?php $count++;
                    ?>
                </div><!-- #post-## -->
            </div>
        <?php endwhile; ?>
    </div>
</div>




