<?php
$grupoFooter = get_fields('options')['grupo_informacoes_para_contato'];
?>
<div class="floating-contacts">
    <a href="tel:<?php echo $grupoFooter['telefones'][0]['numero_telefone'] ?>" target="_blank">
        <div class="floating-contacts-content">
            <h4>Ligar</h4>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/tel.png"/>
        </div>
    </a>
    <a href="https://api.whatsapp.com/send?phone=55<?php echo $grupoFooter['whatsapp'][0]['link_whatsapp'] ?>&text=Ol%C3%A1,%20tudo%20bem?" target="_blank">
        <div class="floating-contacts-content">
            <h4>Whatsapp</h4>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/wpp.png"/>
        </div>
    </a>
    <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
        <div class="floating-contacts-content">
            <h4>Fazer uma simulação</h4>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/simulacao.png"/>
        </div>
    </a>
</div>