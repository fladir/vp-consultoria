<?php
$conteudo = get_field('conteudo_cta_aluguel');
?>
<section id="property-search" class="property-search bg-img4 my-5" style="background-image: url(<?php print_r($conteudo['imagem_de_fundo']['sizes']['banner_cta']) ?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="search_smart_property text-center">
                    <h2><?php echo $conteudo['titulo'] ?></h2>
                    <p><?php echo $conteudo['subtitulo'] ?></p>
                    <a href="<?php echo $conteudo['link_do_botao'] ?>" class="btn booking_btn btn-thm"><?php echo $conteudo['texto_do_botao'] ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
