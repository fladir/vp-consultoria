<?php
$conteudo = get_field('conteudo_fotografar_imovel');

?>

<!-- Modern Apertment -->
<section class="modern-apertment pt100 pb90" style="background-image: url(<?php print_r($conteudo['imagem_de_fundo']['sizes']['banner']) ?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="modern_apertment mt30">
                    <h2 class="title"><?php echo $conteudo['titulo'] ?></h2>
                    <p><?php echo $conteudo['texto'] ?></p>
                    <a class="btn booking_btn btn-thm" href="<?php echo $conteudo['link_do_botao'] ?>"><?php echo $conteudo['texto_do_botao'] ?></a>
                </div>
            </div>
        </div>
    </div>
</section>