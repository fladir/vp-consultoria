<?php
$grupoTopoDaPaginaGeral = get_field('grupo_header', 'options')['imagem_de_fundo_geral'];
$grupoTopoDaPagina = get_field('imagem_de_fundo');
?>

<!-- Inner Page Breadcrumb -->
<section class="inner_page_breadcrumb" style="background-image: url(<?php get_field('imagem_de_fundo')['sizes']['topo'] ? print_r(get_field('imagem_de_fundo')['sizes']['topo']) : print_r(get_field('grupo_header', 'options')['imagem_de_fundo_geral']['sizes']['topo']); ?>)">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="breadcrumb_content">
                    <?php
                    if (function_exists('yoast_breadcrumb')) {
                        yoast_breadcrumb('<p id="breadcrumbs-item">', '</p>');
                    }
                    ?>
                    <h1 class="breadcrumb_title"><?php the_title() ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
