<?php $banner = get_field('conteudo_do_banner');
?>

<section class="home-three" style="background-image: url(<?php print_r($banner['imagem_do_banner']['sizes']['banner']) ?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="home3_home_content">
                    <h1 class="text-uppercase"><?php echo $banner['titulo'] ?></h1>
                    <h3 class="text-white"><?php echo $banner['subtitulo'] ?></h3>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="home_adv_srch_opt home3">
                    <div class="tab-content home1_adsrchfrm" id="pills-tabContent">
                    <?php echo do_shortcode('[mdf_search_form id="247"]'); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
