<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<div class="container">
    <div class="row">
        <h1 class="titulo-destaque">
            <?php if(is_page('orcamento') || is_page(151)){ echo 'Contato'; } else { the_title(); } ?>
        </h1>
        <div class="col-sm-8 offset-sm-2 col-xl-5 offset-xl-5"><?php the_content(); ?></div>
    </div>
</div>
<?php endwhile; endif; ?>