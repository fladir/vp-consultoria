<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<div class="container">
    <div class="row">
        <div class="col-12 header-produto">
            <figure>
                <?php $img_padrao = get_field('grupo_de_configuracoes_gerais', 'options')['imagem_padrao']; ?>
                <?php if(has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('produto-header', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                <?php else: ?>
                    <?php echo wp_get_attachment_image($img_padrao, 'produto-header', array( 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                <?php endif; ?>
            </figure>

            <div class="col-12 col-lg-8 offset-lg-3 col-xl-7 offset-xl-4 mt-5 info-header">
                <h1><?php the_title(); ?></h1>
                <div class="description">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; endif; ?>