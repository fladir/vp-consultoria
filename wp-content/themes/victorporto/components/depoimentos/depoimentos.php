<?php
$args = array(
    'post_type' => 'depoimento',
    'order' => 'ASC',
    'posts_per_page' => -1,
);
$WPQuery = new WP_Query($args);
?>
<section class="our-testimonials">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="main-title text-center mb20">
                    <h2>Depoimentos</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="testimonial_grid_slider style2">
                    <?php while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <div class="item">
                        <div class="testimonial_grid style2">
                            <div class="thumb">
                                <?php the_post_thumbnail('depoimentos'); ?>
                                <div class="tg_quote"><span class="fa fa-quote-left"></span></div>
                            </div>
                            <div class="details">
                                <h4><?php the_title() ?></h4>
                                <p><?php echo get_field('subtitulo_depoimentos'); ?></p>
                                <p class="mt25"><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                        <?php wp_reset_postdata(); ?>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>