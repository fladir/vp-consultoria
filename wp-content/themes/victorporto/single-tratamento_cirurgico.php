<?php
get_header();

?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="tecnicas-cirurgicas">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php the_post_thumbnail(); ?>
                    <div class="mt-4">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="col-md-4">

                    <?php
                    wp_nav_menu(array(
                        'menu' => 'Técnicas Cirúrgicas',
                        'theme_location' => 'tecnicas',
                        'depth' => 3,
                        'container' => 'div',
                        'container_id' => 'menu-tecnicas',
                        'container_class' => 'navbar-tecnicas',
                        'menu_id' => 'navbarTecnicas',
                        'menu_class' => 'nav navbar-nav w-100 ',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker(),
                    ));
                    ?>

                </div>
            </div>

        </div>
    </section>


    <!-- Depoimentos -->
<?php get_template_part('components/index/depoimentos'); ?>

    <!-- Dr. na Mídia -->
<?php get_template_part('components/index/dr-na-midia'); ?>

<?php get_footer(); ?>