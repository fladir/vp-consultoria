<div class="row d-block searchform-wrapper">

    <div class="col-12">
        <form class="form-inline form-blog" id="searchform" method="get" action="<?php echo home_url('/'); ?>">

                <div class="form-group input-group w-100">
                    <input name="s" class="form-control" placeholder="Digite sua busca..." type="text" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" id="pesquisa" />
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><span class="flaticon-magnifying-glass"></span></button>
                    </div>
                </div>

        </form>
    </div>

</div>