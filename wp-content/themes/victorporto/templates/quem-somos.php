<?php
/* Template Name: Quem Somos */
get_header();
$diferenciais = get_field('itens_diferenciais');
?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- About Text Content -->
    <section class="about-section">
        <div class="container">

            <?php the_content(); ?>


            <div class="row mt50">
                <div class="col-lg-6 offset-lg-3">
                    <div class="main-title text-center">
                        <h2><?php echo get_field('titulo_da_seccao') ?></h2>
                        <p><?php echo get_field('subtitulo_da_secao') ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php if ($diferenciais) : foreach ($diferenciais as $diferencial) : ?>
                    <div class="col-md">
                        <div class="why_chose_us style2">
                            <div class="icon">
                                <img src="<?php print_r($diferencial['icone']['sizes']['icone_o_que_procura']) ?>" alt="<?php the_title() ?>" class="icon_diferenciais">
                            </div>
                            <div class="details">
                                <h4><?php echo $diferencial['titulo'] ?></h4>
                                <p><?php echo $diferencial['texto'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
    </section>


    <!-- Depoimentos -->
<?php get_template_part('components/depoimentos/depoimentos'); ?>

<?php get_footer(); ?>