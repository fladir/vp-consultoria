<?php
/* Template Name: Quero Alugar */
get_header();
$grupoFooter = get_fields('options')['grupo_footer'];
$certificados = $grupoFooter['certificados'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
?>
    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Our Contact -->
    <section class="our-contact pb0 bgc-f7">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-xl-8">
                    <div class="form_grid">

                        <?php the_content() ?>
                        <?php echo do_shortcode('[contact-form-7 title="Contato"]'); ?>


                    </div>
                </div>
                <div class="col-lg-5 col-xl-4">
                    <div class="contact_localtion">
                        <h4>Fale Conosco</h4>
                        <div class="content_list">
                            <h5>Endereço</h5>
                            <?php foreach ($enderecos as $endereco) : ?>
                                <p><?php echo $endereco['endereco']; ?></p>
                            <?php endforeach; ?>
                        </div>
                        <div class="content_list">
                            <h5>Telefone</h5>
                            <?php foreach ($telefones as $telefone) : ?>
                                <p><a href="tel:<?php echo $telefone['numero_telefone']; ?>"
                                      target="_blank"><?php echo $telefone['numero_telefone']; ?></a></p>
                            <?php endforeach; ?>
                        </div>
                        <div class="content_list">
                            <h5>E-mail</h5>
                            <?php foreach ($emails as $email) : ?>
                                <p><a href="mailto:<?php echo $email['endereco_email']; ?>"
                                      target="_blank"><?php echo $email['endereco_email']; ?></a></p>
                            <?php endforeach; ?>                        </div>

                        <h5>Siga-nos</h5>
                        <ul class="contact_form_social_area">
                            <?php foreach ($redesSociais as $redeSocial) : ?>

                                <li class="list-inline-item">
                                    <a href="<?php echo $redeSocial['link_social'] ?>" target="_blank"
                                       class="rede-social <?php echo $redeSocial['nome_rede_social'] ?>"
                                       title="<?php echo $redeSocial['nome_rede_social'] ?>">
                                        <i class="<?php echo $redeSocial['icone_social'] ?>"></i>
                                    </a>
                                </li>

                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid p0 mt50">
            <div class="row">
                <div class="col-lg-12">
                    <div class="h100" id="map-canvas"></div>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>