<?php get_header() ?>

    <!-- Banner -->
<?php  get_template_part('components/banner/banner-and-search'); ?>

    <!-- Slider venda -->
<?php  get_template_part('components/index/slider-venda'); ?>

    <!-- O que está procurando -->
<?php  get_template_part('components/index/o-que-voce-esta-procurando'); ?>

    <!-- Slider aluguel -->
<?php  get_template_part('components/index/slider-aluguel'); ?>

    <!-- Fotografar Imóvel -->
<?php  get_template_part('components/index/fotografar-imovel'); ?>

    <!-- CTA Aluguel -->
<?php  get_template_part('components/index/cta-aluguel'); ?>

    <!-- Depoimentos -->
<?php  get_template_part('components/depoimentos/depoimentos'); ?>


<?php get_footer() ?>