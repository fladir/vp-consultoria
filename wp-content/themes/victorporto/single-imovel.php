<?php
get_header();
$tipoImovel = get_field('tipo_de_imovel');
$destaque = get_field('destaque');
$destaque = get_field('destaque');
?>

    <div class="single_page_listing_style">
        <div class="container-fluid p0">
            <div class="row">
                <div class="col-sm-6 col-lg-6 p0">
                    <div class="row m0">
                        <div class="col-lg-12 p0">
                            <div class="spls_style_one pr1 1px" style="height: 574px;">
                                <?php the_post_thumbnail(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6 p0">
                    <div class="row m0">


                        <?php
                        $galeriaImovel = get_field('galeria_imovel');
                        $i = 0;
                        if ($galeriaImovel):

                            ?>

                            <?php foreach ($galeriaImovel as $item):

                            if ($i >= 4) {
                                $class = 'd-none';
                            } ?>
                            <div class="col-sm-6 col-lg-6 p0 <?php echo $class; ?>">
                                <div class="spls_style_one">
                                    <a data-fancybox="gallery" class="popup-img"
                                       href="<?php echo esc_url($item['sizes']['imovel_full']); ?>"><img
                                                class="img-fluid w100"
                                                src="<?php echo esc_url($item['sizes']['imovel_small']); ?>"
                                                alt="ls2.jpg"></a>
                                </div>
                            </div>
                            <?php $i++;
                        endforeach; ?>

                        <?php endif; ?>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="p0">
        <div class="container">
            <div class="row listing_single_row">
                <div class="col-sm-6 col-lg-7 col-xl-8">
                    <div class="single_property_title">
                        <a data-fancybox="gallery" href="<?php echo get_the_post_thumbnail_url($post_id); ?>"
                           class="upload_btn popup-img"><span
                                    class="flaticon-photo-camera"></span> Ver Fotos</a>
                        <?php
                        $galeriaImovel = get_field('galeria_imovel');
                        $i = 0;
                        if ($galeriaImovel):

                            ?>

                            <?php foreach ($galeriaImovel as $item):

                            if ($i >= 4) {
                                $class = 'd-none';
                            } ?>

                            <a data-fancybox="gallery" class="d-none popup-img"
                               href="<?php echo esc_url($item['sizes']['imovel_full']); ?>"><img
                                        class="img-fluid w100"
                                        src="<?php echo esc_url($item['sizes']['imovel_small']); ?>"
                                        alt="ls2.jpg"></a>

                            <?php $i++;
                        endforeach; ?>

                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-5 col-xl-4">
                    <div class="single_property_social_share">
                        <div class="spss style2 mt10 text-right tal-400">
                            <ul class="mb0">
                                <li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a>
                                </li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-share"></span></a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-printer"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Agent Single Grid View -->
    <section class="our-agent-single bgc-f7 pb30-991">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="listing_sidebar dn db-991">
                        <div class="sidebar_content_details style3">
                            <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
                            <div class="sidebar_listing_list style2 mobile_sytle_sidebar mb0">
                                <div class="sidebar_advanced_search_widget">
                                    <h4 class="mb25">Advanced Search <a class="filter_closed_btn float-right"
                                                                        href="#"><small>Hide Filter</small> <span
                                                    class="flaticon-close"></span></a></h4>
                                    <ul class="sasw_list style2 mb0">
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName1"
                                                       placeholder="keyword">
                                                <label for="exampleInputEmail"><span
                                                            class="flaticon-magnifying-glass"></span></label>
                                            </div>
                                        </li>
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputEmail"
                                                       placeholder="Location">
                                                <label for="exampleInputEmail"><span
                                                            class="flaticon-maps-and-flags"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Status</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Property Type</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="small_dropdown2">
                                                <div id="prncgs" class="btn dd_btn">
                                                    <span>Price</span>
                                                    <label for="exampleInputEmail2"><span
                                                                class="fa fa-angle-down"></span></label>
                                                </div>
                                                <div class="dd_content2">
                                                    <div class="pricing_acontent">
                                                        <input type="text" class="amount" placeholder="$52,239">
                                                        <input type="text" class="amount2" placeholder="$985,14">
                                                        <div class="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bathrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bedrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Garages</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Year built</option>
                                                        <option>2013</option>
                                                        <option>2014</option>
                                                        <option>2015</option>
                                                        <option>2016</option>
                                                        <option>2017</option>
                                                        <option>2018</option>
                                                        <option>2019</option>
                                                        <option>2020</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="min_area style2 list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName2"
                                                       placeholder="Min Area">
                                            </div>
                                        </li>
                                        <li class="max_area list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName3"
                                                       placeholder="Max Area">
                                            </div>
                                        </li>
                                        <li>
                                            <div id="accordion" class="panel-group">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a href="#panelBodyRating" class="accordion-toggle link"
                                                               data-toggle="collapse" data-parent="#accordion"><i
                                                                        class="flaticon-more"></i> Advanced features</a>
                                                        </h4>
                                                    </div>
                                                    <div id="panelBodyRating" class="panel-collapse collapse">
                                                        <div class="panel-body row">
                                                            <div class="col-lg-12">
                                                                <ul class="ui_kit_checkbox selectable-list float-left fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck1">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck1">Air
                                                                                Conditioning</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck4">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck4">Barbeque</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck10">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck10">Gym</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck5">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck5">Microwave</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck6">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck6">TV Cable</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck2">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck2">Lawn</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck11">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck11">Refrigerator</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck3">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck3">Swimming
                                                                                Pool</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <ul class="ui_kit_checkbox selectable-list float-right fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck12">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck12">WiFi</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck14">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck14">Sauna</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck7">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck7">Dryer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck9">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck9">Washer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck13">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck13">Laundry</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck8">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck8">Outdoor
                                                                                Shower</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck15">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck15">Window
                                                                                Coverings</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_button">
                                                <button type="submit" class="btn btn-block btn-thm">Search</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-8 mt50">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="listing_single_description2 mt30-767 mb30-767">
                                <div class="row">
                                    <div class="col-md-8 single_property_title">
                                        <h2><?php the_title() ?></h2>
                                        <p>
                                            <?php if (get_field('endereco')): ?>
                                                <?php echo get_field('endereco') ?>,
                                            <?php endif; ?>


                                            <?php
                                            $bairros = get_field('bairro');
                                            if ($bairros): ?>
                                                <?php foreach ($bairros as $bairro): ?>
                                                    <strong>Bairro:</strong> <?php echo $bairro ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                            <?php echo get_field('regiao') ?>

                                            <?php echo get_field('cep') ?>

                                            <?php if (get_field('cidade')): ?>
                                                <strong>Cidade:</strong> <?php echo get_field('cidade') ?>
                                            <?php endif; ?><?php echo get_field('estado') ?>

                                        </p>
                                    </div>
                                    <div class="col-md-4 single_property_social_share style2">
                                        <div class="price">
                                            <h2>R$ <?php echo get_field('preco') ?>
                                                <small><?php echo $tipoImovel == 'Alugar' ? '/mês' : '' ?></small></h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="listing_single_description style2">
                                <div class="lsd_list">
                                    <ul class="mb0">
                                        <?php if (get_field('codigo')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Código: </strong><?php echo get_field('codigo') ?></a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('area')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Área: </strong><?php echo get_field('area') ?> m²</a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('condominio')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Condomínio: </strong><?php echo get_field('condominio') ?>
                                                </a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('quartos')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Quartos: </strong><?php echo get_field('quartos') ?></a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('suites')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Suítes: </strong><?php echo get_field('suites') ?></a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('banheiros')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Banheiros: </strong><?php echo get_field('banheiros') ?></a>
                                            </li>
                                        <?php endif; ?>
                                        <?php if (get_field('lugares_garagem')): ?>
                                            <li class="list-inline-item">
                                                <a><strong>Vagas: </strong><?php echo get_field('lugares_garagem') ?>
                                                </a></li>
                                        <?php endif; ?>
                                        <?php if (get_field('area_privativa') == 1): ?>
                                            <li class="list-inline-item"><a><strong>Área Privativa: </strong>Sim</a>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <h4 class="fw-bold">Descrição</h4>
                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <?php the_content(); ?>
                                <?php endwhile; endif; ?>


                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="application_statics mt30">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 class="mb10">Diferenciais</h4>
                                    </div>
                                    <?php
                                    $teste = get_field('caracteristicas_do_imovel')['caracteristica_imovel'];
                                    #print_r($teste) ?>
                                    <ul class="row order_list list-inline-item">
                                        <?php
                                        foreach ($teste as $tset) {
                                            ?>
                                            <li class="col-lg-6 col-sm-6 col-xs-12">
                                                <a>
                                                    <span class="flaticon-tick"></span>
                                                    <?php echo $tset; ?>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4 mt50">


                    <div class="sidebar_listing_list">
                        <h3 class="text-center mb-4">Tenho interesse neste imóvel
                        </h3>
                        <?php echo do_shortcode('[contact-form-7 title="Contato"]'); ?>
                    </div>

                    <div class="sidebar_feature_listing">
                        <h3 class="mb-5">Imóveis Semelhantes</h3>

                        <?php
                        $currentRoomsNumber = intval(get_field('quartos'));
                        $taxonomy_type_imovel = get_the_terms(get_the_ID(), 'imoveis');

                        $id_taxonomy_type_imovel = isset($taxonomy_type_imovel[0]->term_id) ? $taxonomy_type_imovel[0]->term_id : null;
                        $imoveis_semelhates = new WP_Query(array(
                            'post_type' => 'imovel',
                            'orderby' => 'rand',
                            'posts_per_page' => 3,
                            'meta_query' => array(
                                array(
                                    'key' => 'quartos',
                                    'value' => $currentRoomsNumber,
                                    'compare' => '=',
                                    'type' => 'NUMERIC',
                                ),
                            )
                        ));

                        if (!$currentRoomsNumber) {
                            $imoveis_semelhates = new WP_Query(array(
                                'post_type' => 'imovel',
                                'orderby' => 'rand',
                                'posts_per_page' => 3,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'imoveis',
                                        'terms' => $id_taxonomy_type_imovel
                                    )
                                )
                            ));
                        }

                        ?>

                        <!-- Feature block -->
                        <?php if ($imoveis_semelhates->have_posts()) { ?>
                            <?php while ($imoveis_semelhates->have_posts()) {
                                $imoveis_semelhates->the_post();
                                $quartos = get_field('quartos');
                                ?>

                                <div class="media">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php the_post_thumbnail('imoveis_semelhantes', array('class' => 'align-self-start mr-3')); ?>
                                    </a>
                                    <div class="media-body">
                                        <a href="<?php the_permalink(); ?>">
                                            <h5 class="mt-0 post_title"><?php the_title() ?></h5>
                                        </a>
                                        <a href="<?php the_permalink(); ?>">R$ <?php echo get_field('preco') ?>
                                            <small><?php echo $tipoImovel == 'Alugar' ? '/mês' : '' ?></small></a>
                                        <ul class="mb0">
                                            <li class="list-inline-item">
                                                Quartos: <?php echo get_field('quartos') ?></li>
                                            <li class="list-inline-item">
                                                Banheiros: <?php echo get_field('banheiros') ?></li>
                                        </ul>
                                    </div>
                                </div>

                            <?php } ?>
                        <?php } else { ?>
                            <p>Nenhum imovel para exibir.</p>
                        <?php } ?>
                        <?php wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>