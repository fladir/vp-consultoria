<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo', 292, 99, true); // Logo
    add_image_size('banner', 1920, 650, true); // Slides
    add_image_size('topo', 1920, 500, true); // Topo das Páginas
    add_image_size('slide_mobile', 410, 630, true); // Slide Mobile
    add_image_size('col_6', 960, 630, true); // Coluna (Metade)
    add_image_size('img_full', 1920, 520); // Imagem Fullwidth
    add_image_size('img_post_list', 338, 228, true); // Imagem Posts List
    add_image_size('fundo_secao', 1920, 690, true); // Fundo Seção
    add_image_size('icone_o_que_procura', 60, 60); // Ícone O que está procurando
    add_image_size('banner_cta', 1600, 450, true); // Ícone O que está procurando
    add_image_size('imoveis_home', 262, 358, true); // Slider imóveis home
    add_image_size('depoimentos', 114, 114, true); // Depoimentos
    add_image_size('imovel_small', 483, 302, true); // Imagem Imóvel Pequena
    add_image_size('imovel_full', 960, 600, true); // Imagem Imóvel Completa
    add_image_size('imoveis_semelhantes', 90, 80, true); // Imagens Semelhantes
    add_image_size('imoveis_list', 284, 217, true); // Lista de Imóveis
    add_image_size('single_post', 670, 380, true); // Featured img single post



}