<div class="wrap">
    <h2>Get News Letter Mail</h2>
    <p  class="alert alert-primary" role="alert">
        Use o código abaixo para exibir o formulario de news letter :
    </p>
    <code>
        [get_news_letter_mail mail="Email" buttom="Submit"]
    </code>
    <form action="" method="post" enctype="multipart/form-data">
        <?php if (isset($mensagem) && $mensagem != '') : ?>
            <p>
                <?php echo nl2br($mensagem); ?>
            </p>
        <?php endif; ?>
        <div class="inputs">
            <h3>Archive Name</h3>
            <label for="archive_name">
                <input type="text" placeholder="Nome do Arquivo" name="archive_name" value="<?php echo isset($options_default['archive_name']) ? $options_default['archive_name'] : ''; ?>">
            </label>
            <h3>Archive Type</h3>
            <label for="archive_type">
                <select name="archive_type" id="">
                    <option value="xml" <?php echo isset($options_default['archive_name']) && $options_default['archive_type'] == 'xml' ? 'selected' : ''; ?> >XML</option>
                    <option value="csv" <?php echo isset($options_default['archive_name']) && $options_default['archive_type'] == 'csv' ? 'selected' : ''; ?> >CSV</option>
                </select>
            </label>
        </div>
        <div class="table">
            <table class="wp-list-table widefat striped tags">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Lista</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if(isset($all) && $all){
                        foreach($all as $a){
                            ?>
                            <tr>
                                <td><?php echo $a['news_letter_id']; ?></td>
                                <td><?php echo $a['news_letter_name']; ?></td>
                                <td><?php echo $a['news_letter_mail']; ?></td>
                                <td><?php echo $a['news_letter_list']; ?></td>
                                <td><a href="?page=get-news-letter-mail&delete=<?php echo $a['news_letter_id']; ?>" class="btn-delete">Excluir</a></td>
                            </tr>
                            <?php
                        }
                    }else{
                        ?>
                        <tr><td colspan="5">Nada Encontrado.</td></tr>
                        <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
        <?php submit_button('Generate'); ?>
    </form>
</div>
<?php
    if(isset($generate)){
        ?>
        <script>
            jQuery(document).ready(function(){
                var x=new XMLHttpRequest();
                x.open("GET", "<?php echo $options_default['url']; ?>", true);
                x.responseType = 'blob';
                x.onload=function(e){download(x.response, "<?php echo $options_default['archive_name']; ?>.<?php echo $options_default['archive_type']; ?>", "text/<?php echo $options_default['archive_type']; ?>" ); }
                x.send();
            });
        </script>
        <?php
    }
?>
